# Elastic mapping analyze script

****_Warning: coding softwares is not my area of work. This script only exists because Elasticsearch currently lacks a feature to efficiently optimize mappings and index templates. An enhancement request has already been opened on their side but in the meantime, this script allows to perfom small optimization on mappings._****

Please note that this script is currently in a really early stage. Over the 3 forseen features, only one  is available for now:
- [X] **Minimal mapping :** the script analyzes aliases and underlying indices to search for the exhaustive mapping and check the existence of each field in at least one document. Then, a minimal mapping is built with the existing fields only.
- [ ] Low cardinality fields
- [ ] Orphan fields

## Install
1. Download the source code
2. (Optionnal) setup virtualenv to be in a clean environment:
`virtualenv -p python3.7 venv` then `source venv/bin/activate`
3. Install project dependencies: `pip install -r requirements.txt`

## Configure
Copy the `config_sample.yml` to `my_config.yml`.
Edit the `my_config.yml` file and define appropriate configurations:
- Cluster connection
- Features you want to use
    - Minimal mapping : specify the list of aliases (it also works with indices directly) that should be minimized.

## Run
Once configured, you just have to run the script with `python app.py`

## Known issues
1. Folder path for the results is not created in advance, you can fix this with: `mkdir -p results/mapping` before running the `app.py`
