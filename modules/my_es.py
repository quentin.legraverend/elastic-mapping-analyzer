from elasticsearch import Elasticsearch

class MyEs(object):
    def __init__(self, hostname, port, ssl, username, password):
        self.hostname = hostname
        self.port = port
        self.host_list = [{
                'host': h,
                'port': self.port
            } for h in self.hostname.split(',')]
        if ssl:
            self.es = Elasticsearch(self.host_list,
                        http_auth = (username, password),
                        use_ssl = True,
                        verify_certs = False,
                        ssl_show_warn = False
                        )
        else:
            self.es = Elasticsearch(self.host_list)

    def does_field_exist(self, index, field):
        result = self.es.search(
            index = index,
            body  = {
                      "query": {
                        "bool": {
                          "must": [
                            {
                              "exists": {
                                "field": field
                              }
                            }
                          ]
                        }
                      }
                    }
        )
        if result['hits']['total']['value'] == 0:
            return False
        else:
            return True

    def get_mapping(self, index):
        return self.es.indices.get_mapping(
            index = index
        )

    def get_all_lifecycle_policies(self):
        return self.es.ilm.get_lifecycle()