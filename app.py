import os
import json

from datetime import datetime

from features import build_minimal_mapping
from features import write_policies_to_csv
from modules import MyEs
from settings import load_configuration

def main():
    config_file = os.getenv('CONFIG_FILE', 'my_config.yml')
    print('{} - [ETB] starting with config file {}'.format(datetime.now(), config_file))
    config = load_configuration(config_file)

    my_es = MyEs(
    hostname = config['elasticsearch']['host'],
    port = config['elasticsearch']['port'],
    ssl = config['elasticsearch']['ssl'],
    username = config['elasticsearch']['username'],
    password = config['elasticsearch']['password']
    )

    if config['minimal_mapping']['enabled']:
        print('{} - [ETB] Minimal mapping function STARTING'.format(datetime.now()))
        for alias in config['minimal_mapping']['alias_list']:
            file_name = alias +'_minimal_mapping.json'
            file = open('results/mapping/'+file_name, 'w')
            file.write(build_minimal_mapping(my_es, alias))
            file.close()
            print('{} - [ETB] Minimal mapping for alias {} written to disk'.format(datetime.now(), alias))

    if config['orphan_fields']['enabled']:
        print('{} - [ETB] Orphan fields function NOT AVAILABLE'.format(datetime.now()))

    if config['low_cardinality_fields']['enabled']:
        print('{} - [ETB] Low cardinality fields function NOT AVAILABLE'.format(datetime.now()))

    if config['lifecycle_policy_checker']['enabled']:
        print('{} - [ETB] Lifecyle Policy Checker function STARTING'.format(datetime.now()))
        write_policies_to_csv(my_es)
        print('{} - [ETB] Policy check written to disk'.format(datetime.now()))

if __name__ == '__main__':
    main()
