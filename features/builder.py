import json

from functools import reduce

from .functions import get_global_mapping
from .functions import get_unused_field_list
from .functions import get_unused_field_path
from .functions import remove_field_from_mapping

def build_minimal_mapping(my_es, alias):
    unused_field_list = get_unused_field_list(my_es, alias)
    unused_field_path = get_unused_field_path(unused_field_list)
    global_mapping = get_global_mapping(my_es, alias)
    return json.dumps(remove_field_from_mapping(global_mapping, unused_field_path), sort_keys=True, indent=2)
