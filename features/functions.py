import collections
import json

from datetime import datetime

def get_global_mapping(my_es, alias):
    global_mapping = {}
    for index in my_es.get_mapping(alias):
        merge_dict(global_mapping, my_es.get_mapping(alias)[index]['mappings']['properties'])
    return global_mapping

def get_index_field_list(index):
    field_list = {}
    search_field_path(index['mappings']['properties'], field_list, '')
    return field_list

def get_unused_field_list(my_es, alias):

    unused_field_list = []
    used_field_list = []

    for index in my_es.get_mapping(alias):
        print('{} - [ETB] List of unused fields for index {}'.format(datetime.now(), index))
        field_list = get_index_field_list(my_es.get_mapping(alias)[index])
        for field in field_list:
            if field not in (unused_field_list + used_field_list):
                if not my_es.does_field_exist(alias, field):
                    print('{} - NOT USED'.format(field))
                    unused_field_list += [field]
                else:
                    print('{} - USED'.format(field))
                    used_field_list += [field]
    total_number_of_fields = len(unused_field_list + used_field_list)
    saved_space = (total_number_of_fields-len(used_field_list))/total_number_of_fields*100
    print('{} - [ETB] {}% space saved: number of fields decrease from {} to {}'.format(datetime.now(), int(saved_space), total_number_of_fields, len(used_field_list)))
    return unused_field_list

def get_unused_field_path(unused_field_list):
    unused_field_path = []
    for field in unused_field_list:
        unused_field_path.append(field.replace('.', '.properties.'))
    return unused_field_path

# source: https://stackoverflow.com/a/10704003/4295912
def merge_dict (d1, d2):
    """
    Modifies d1 in-place to contain values from d2.  If any value
    in d1 is a dictionary (or dict-like), *and* the corresponding
    value in d2 is also a dictionary, then merge them in-place.
    """
    for k,v2 in d2.items():
        v1 = d1.get(k) # returns None if v1 has no value for this key
        if ( isinstance(v1, collections.Mapping) and
             isinstance(v2, collections.Mapping) ):
            merge_dict(v1, v2)
        else:
            d1[k] = v2

# source: https://stackoverflow.com/a/69181616/4295912
def remove_field_from_mapping(mapping, field_list=None, path=None):
    minimal_mapping = {}

    if type(mapping) != dict:
        return mapping
    if field_list is None:
        field_list = []
    if path is None:
        path = ""

    for k, v in mapping.items():
        subpath = f"{path}.{k}" if path != "" else k
        if subpath not in field_list:
            subnode = remove_field_from_mapping(v, field_list, subpath)
            if subnode != {}:
                minimal_mapping [k] = subnode

    return minimal_mapping

def search_field_path(object, field_list, root_key):
    for item in object:
        if 'properties' in object[item].keys():
            search_field_path(object[item]['properties'], field_list, root_key + item + '.')
            field_list.update({root_key + item : object[item]})
        else:
            field_list.update({root_key + item : object[item]})
