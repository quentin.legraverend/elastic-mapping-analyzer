import json
import csv

def write_policies_to_csv(my_es):
    policies = my_es.get_all_lifecycle_policies()
    with open('results/policies/list.csv', 'w') as policy_csv:
        policy_writer = csv.writer(policy_csv, delimiter=',', quotechar='"',quoting=csv.QUOTE_MINIMAL)
        policy_writer.writerow(['Policy name', 'Hot max size', 'Hot max age', 'Delete min age'])
        for k, v in policies.items():
            actions = v['policy']['phases']['hot']['actions']
            if "rollover" in actions:
                if "max_age" in actions['rollover']:
                    hot_max_age = actions['rollover']['max_age']
                else:
                    hot_max_age = "NA"
                if "max_primary_shard_size" in actions['rollover']:
                    hot_max_size = actions['rollover']['max_primary_shard_size']
                elif "max_size" in actions['rollover']:
                    hot_max_size = actions['rollover']['max_size']
                else:
                    hot_max_size = 'NA'
            else:
                hot_max_age = "NA"
            if "delete" in v['policy']['phases']:
                 delete_min_age = v['policy']['phases']['delete']['min_age']
            else:
                delete_min_age = "NA"
            policy_writer.writerow([k, hot_max_size, hot_max_age, delete_min_age])
